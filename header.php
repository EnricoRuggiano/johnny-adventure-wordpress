<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
    <title><?php bloginfo( 'name' ); ?></title>
    <?php wp_head() ?>
</head>

<body <?php body_class(); ?>>

<div class='ja-background-black'></div>
<div class='ja-background'>
    <img src='https://www.johnadventure.net/wp-content/themes/johnny-adventure-wordpress/assets/img/background.svg'/>
</div>

<div class="container">
    <header class="site-header">
        <!-- <h1><a href="<?php echo home_url(); ?>"></a>Johnny Adventure</h1> -->
    </header>    