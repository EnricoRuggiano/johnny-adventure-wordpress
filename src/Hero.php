<?php

function Hero()
{
    $path = get_template_directory_uri();
    $text = <<<HTML

<div class="hero">
  <div class="hero-title">
    <h1><a href="<?php echo home_url(); ?>"></a>Johnny Adventure</h1>
  </div>

  <div class="hero-description">
    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <p><br></p>
    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <p><br></p>
    <h4> Let's go!</h4>
  </div>

  <div class='hero-links'>
    <a href='https://www.patreon.com/johnnyadventure' target="_blank">
        <img class='hero-button' src='$path/assets/img/icon_patreon.png'>
    </a>
    <a href='https://game.johnadventure.net' target="_blank">
        <img class='hero-button' src='$path/assets/img/icon_ibm.png'>
    </a>
  </div>

</div>

HTML;

    echo $text;
}
