<?php

function Drawers()
{
    $path = get_template_directory_uri();
    $text = <<<HTML

<script> 
    function navigate(url) {
        window.location.href = url;
    }
</script>

<div class="drawers">

<!-- 1.0 -->
  <div class="drawer" role="button" onclick="navigate('news')" >
    <div class='drawer-icon'> 
        <img src='$path/assets/img/icon_folders.svg' width=125px height=150px/>
    </div>
    <div class='drawer-text' role="button" onclick="navigate('develop')">
        <h3> News </h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
    </div>
  </div>

<!-- 2.0 -->
  <div class="drawer" role="button" onclick="navigate('develop')">
    <div class='drawer-icon'> 
        <img src='$path/assets/img/icon_amaro.svg' width=125px height=150px/>
    </div>
    <div class='drawer-text'>
        <h3> Inside The Stuff! </h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
    </div>
  </div>

<!-- 3.0 -->
  <div class="drawer" role="button" onclick="navigate('support')">
    <div class='drawer-icon'> 
        <img src='$path/assets/img/icon_money.png' style='scale: 0.85;'/>
    </div>
    <div class='drawer-text'>
        <h3> Support </h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
    </div>
  </div>

</div>

HTML;

    echo $text;
}
