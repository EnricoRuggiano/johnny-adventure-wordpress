<?php

function Gallery()
{
    $path = get_template_directory_uri();
    $text = <<<HTML

<div class="swiper gallery">
  <div class="swiper-wrapper">

    <div class="swiper-slide gallery-item">
        <img class="slide-image " src="$path/assets/img/preview_bar.jpg");/>
    </div>
    <div class="swiper-slide gallery-item">
        <img class="slide-image " src="$path/assets/img/preview_drug.jpg");/>
    </div>
    <div class="swiper-slide gallery-item">
        <img class="slide-image " src="$path/assets/img/preview_harem.jpg"); />
    </div>
 
</div>
  <div class="swiper-pagination"></div>
  <div class="swiper-button-prev" ></div>
  <div class="swiper-button-next" ></div>
</div>

<script>
    var swiper = new Swiper('.gallery', {
        spaceBetween: 10,
        loop: true,

        effect: "coverflow",
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: "auto",
        coverflowEffect: {
          rotate: 0,
          scale: 0.75,
          stretch: 0,
          depth: 0,
          modifier: 1,
          slideShadows: true,
        },

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            dynamicBullets: true,
        },
    
        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
</script>

HTML;

    echo $text;
}
