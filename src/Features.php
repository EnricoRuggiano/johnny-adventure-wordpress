<?php

function Features()
{
    $path = get_template_directory_uri();
    $text = <<<HTML

<div class="features">

    <div class="features-title">
        <h3>Features</h3>
    </div>
    
    <div class='features-description'>
        <div class='features-list'>
            <ul>
                <li> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p></li>
                <br>
                <li> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p></li>
                <br>
                <li> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p></li>
                <br>
                <li> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p></li>
            </ul>
        </div>
        <div class='features-img'>
            <img src='$path/assets/img/img_cora.svg' width=300px/>
        </div>
  </div>

</div>

HTML;

    echo $text;
}
