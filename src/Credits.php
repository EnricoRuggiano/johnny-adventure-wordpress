<?php

function Credits()
{
    $path = get_template_directory_uri();
    $text = <<<HTML

<div class="credits">

    <div class="credits-logo">
        <img src='$path/assets/img/icon_logo_txt.svg', width='200px' />
    </div>
    <div class="credits-legal">
        <p> Copyright NewareApps 2021 - all rights reserved!</p>
    </div>

    <div class="credits-social">
        <a href='https://www.patreon.com/johnnyadventure' target="_blank">
            <img class='credits-social-icon' src='$path/assets/img/icon_patreon.png'>
        </a>
        <a href='https://www.twitch.tv/lalalaciccio' target="_blank">
            <img class='credits-social-icon' src='$path/assets/img/icon_twitch.png'>
        </a>
        <a href='https://www.deviantart.com/lalalaciccio' target="_blank">
            <img class='credits-social-icon' src='$path/assets/img/icon_deviantart.png'>
        </a>
        <a href='https://www.pinterest.com/LalalaCiccio/' target='_blank'>    
            <img class='credits-social-icon' src='$path/assets/img/icon_pinterest.png'>
        </a>
    </div>

</div>

HTML;

    echo $text;
}
