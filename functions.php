<?php

// Composer 
if ( is_readable( __DIR__ . '/vendor/autoload.php' ) ) {
    require __DIR__ . '/vendor/autoload.php';
}

// php
require_once( get_stylesheet_directory() . '/src/Gallery.php');
require_once( get_stylesheet_directory() . '/src/Hero.php');
require_once( get_stylesheet_directory() . '/src/Drawers.php');
require_once( get_stylesheet_directory() . '/src/Features.php');
require_once( get_stylesheet_directory() . '/src/Credits.php');

// css
function css() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'bootstrap_css',  get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_enqueue_style( 'swiper_css',     get_template_directory_uri() . '/assets/css/swiper-bundle.min.css' );

    wp_enqueue_style( 'gallery_css',    get_template_directory_uri() . '/assets/css/Gallery.css' );
    wp_enqueue_style( 'hero_css',       get_template_directory_uri() . '/assets/css/Hero.css' );
    wp_enqueue_style( 'drawers_css',    get_template_directory_uri() . '/assets/css/Drawers.css' );
    wp_enqueue_style( 'features_css',   get_template_directory_uri() . '/assets/css/Features.css' );
    wp_enqueue_style( 'credits_css',    get_template_directory_uri() . '/assets/css/Credits.css' );
    wp_enqueue_style( 'blog_css',    get_template_directory_uri() . '/assets/css/Blog.css' );

}

// js
function js() {
    wp_enqueue_script( 'jquery_js',     get_template_directory_uri() . '/assets/js/jquery-3.2.1.slim.min.js' );
    wp_enqueue_script( 'popper_js',     get_template_directory_uri() . '/assets/js/popper.min.js' );
    wp_enqueue_script( 'bootstrap_js',  get_template_directory_uri() . '/assets/js/bootstrap.min.js' );
    wp_enqueue_script( 'swiper_js',     get_template_directory_uri() . '/assets/js/swiper-bundle.min.js' );
 }

add_action( 'wp_enqueue_scripts', 'css' );
add_action( 'wp_enqueue_scripts', 'js');