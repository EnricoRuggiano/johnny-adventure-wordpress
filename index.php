<?php /* Template Name: johnny_blog */ ?>
<?php get_header(); ?>

<?php
	if ( have_posts() ) :
	
		while ( have_posts() ) : the_post(); ?>
	
			<article class="post" <?php post_class(); ?>>
			
				<header class="entry-header">
                    <?php the_title( '<h4 class="entry-title">', '</h4>' ); ?>
                    <p><?php the_date(); ?></p>

				</header>
			
				<div class="entry-content">
					<?php the_content( esc_html__( 'Continue reading &rarr;', 'my-custom-theme' ) ); ?>
				</div>
			
			</article>
			
			<?php endwhile;
	
	else :
		?>
		<article class="no-results">
			
			<header class="entry-header">
				<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'my-custom-theme' ); ?></h1>
            </header>
		
			<div class="entry-content">
				<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'my-custom-theme' ); ?></p>
			</div>
		
		</article>
	<?php
	endif;

    // add footer
    get_footer();
	?>
</div>